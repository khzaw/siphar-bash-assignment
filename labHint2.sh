#!/usr/local/bin/bash
#display main ticketing menu
display_menu()
{
  # Called by run_menu
  echo
  echo -e "\e[1;37m--EVERYTHING IS TORRENTED THEATRE’s Self-service Ticketing--   	          	 ===========================================================\e[0m"
  echo "-----------------------------------------------------------"
  echo ""





  echo -n "1. "
  echo -e '\e[1;32mList all Movies and Show time\e[0m'
  echo -n "2. "
  echo -e '\e[1;32mFast Booking \e[0m'
  echo -n "3. "
  echo -e '\e[1;32mSelect seats manually \e[0m'
  echo -n "4. "
  echo -e '\e[1;32mSearch by Screening Time or Movie title \e[0m'
  echo -n "5. "
  echo -e '\e[1;31mQuit\e[0m'
	
  echo -en "Please enter your choice: "
  
  
  
}

# List out available movies
list_movies()
{
	# Called by run_menu
	
	old_IFS=$IFS 
	# separator is defined as carriage return
	IFS=$'\n'
	print_movie_header
	# Reading out line by line from file and Replace YES with TICKET AVAILABLE and Replace No with SOLD OUT text before displaying
	for LINE in `sed -e 's/Yes/Ticket Available/g' -e 's/No/SOLD OUT/g' movielist.txt`  
	do
		print_movie $LINE
	done
	echo ""
	echo ""
  IFS=$old_IFS 
}

# Fast booking
# Select by movie
#bookmovie_fast()
#{
	

#}

	
# Output header for movie listing
# Catered for both numbered and non-numbered list
print_movie_header()
{
	echo -e '\e[1;33m'
	if [ $# == "1" ];then
		echo -n "   "
	fi
	echo -n -e 'Movie Title'
  set_colwidth 'Movie Title' 40
	echo -n -e 'Show Time\t'
  set_colwidth 'Show Time' 5
	if [ $# == "1" ];then
		echo -n -e "\t"
	fi
	echo -n 'Vacancy'
	echo -e '\e[0m'
}

# Set the width of column
# $1 is the text in the column 
# and $2 is the intended column width
set_colwidth()
{
	# String either X or space
	STRING=$1
	# length of string
	LENGTH=${#STRING}
	# creating spacing between each Xs...
	for (( i=$LENGTH; i<=$2; i++))
	do 
		echo -n " "
	done 

}

# Output movie entry
print_movie()
{
	# Reading in line by line and extract the relevant data and assign to the corresponding variable
	TIME=`echo $1 | cut -f1 -d ","`
	DATE=`echo $1 | cut -f2 -d ","`
	TITLE=`echo $1 | cut -f3 -d ","`
	AVAILABLE=`echo $1 | cut -f4 -d ","` 

	echo -n -e "\e[1;36m$TITLE\e[0m" 
	set_colwidth $TITLE 40
	echo -n -e "\e[1;37m$TIME"
	echo -n -e "\e[1;37m-$DATE \e[0m"'\t'
	set_colwidth $DATE 5
  echo -n -e "\e[1;35m$AVAILABLE\e[0m" 
	echo ""

}


# Hint2
# Get current seating arrangement of 
# selected movie from seating.txt
# movie_data($1=Movie number)
movie_data()
{
	COUNT=1
	old_IFS=$IFS 
	IFS=$'\n'
	for LINE in `cat seating.txt`  
	do
		# loop till right movie line and extract only that particular movie's seats information		
		if test $1 -eq $COUNT
			then
				display_seats $LINE $2 # $2 = 

		fi
		THEATER[$COUNT]=$LINE
		let COUNT=COUNT+1
		
	done
  IFS=$old_IFS 	
}

# Mark occupied seat with a X and
# seats selected by user with a red X
# grep -c is count the number of match pattern.
# mark_seat (seatTaken, curCol, curRow, UserSelectedSeatPos)
# mark_seat $1 $CURR_COL $x $2  (1st arg is  
mark_seat()
{
	# Called by display_seats
	
	if [ $(echo "$1" | grep -c "$2$3") -gt 0 ]
	then
			echo -n "X" 
		  set_colwidth "" 5
	elif [ $(echo "$4" | grep -c "$2$3") -gt 0 ]
	then
			echo -en '\e[1;34mX\e[0m'
		  set_colwidth "" 5
	else
			set_colwidth "" 6 
	fi
}

# Print out theater's seating arrangement
display_seats()
{
	COL=5
	ROW=4
	echo

	echo "---------------------------------------"
	echo -e '\e[1;34;47m----------------SCREEN-----------------\e[0m'
	echo "---------------------------------------"
	for (( x=0; x<=$ROW; x++))
	do 
	echo -n "$x" | tr "0" " "
		for (( j=0; j<=$COL; j++))
		do 
			if [ $x == "0" ]
			then
				#Print First line A B C D E labeling as column increment $j
				echo -n $j | tr "012345" " ABCDE"
				set_colwidth "" 5
			else
				#if echo 5 | tr "012345" " ABCDE" ===> output is E
				CURR_COL=$(echo $j | tr "12345" "ABCDE")
				if [ $j == "0" ]
				then
					set_colwidth "" 6
				else
					mark_seat $1 $CURR_COL $x $2
				fi
			fi
	done 
  echo ""
	done
	echo "---------------------------------------"
}


# Select and book seat manually
select_seat()
{
	# Called by run_menu
	
	COUNT=0
	echo
	echo "-- Select A Seat Manually --"
	echo
	old_IFS=$IFS 
	IFS=$'\n'
	print_movie_header "numbered"
	for LINE in `sed -e 's/Yes/Ticket Available/g' -e 's/No/SOLD OUT/g' movielist.txt `;do
		let COUNT=COUNT+1
		echo -n "$COUNT. "
		print_movie $LINE
	done
	IFS=$old_IFS 
	echo
	
	# Select movie to book
	echo -en "Select Movie To Book (Enter 1 to ${COUNT}): "
	read NUM
	NUMERIC=0
	while [ "$NUMERIC" != "1" ];do
		for (( x=$COUNT; x>=1; x--))
		do
			if [ $NUM == "$x" ];then
				NUMERIC=1
				
				
			fi
		done

		if [ "$NUMERIC" != "1" ];then
			echo -en "Invalid Movie Selected, Try Again (Enter 1 to ${COUNT}): "
			read NUM
		fi
	done

	movie_data $NUM
	

	echo -en "Select preferred seat: "
	read seat

	validSeats=""
	count=0

  
  seats=`sed -n ${NUM},${NUM}p seating.txt | cut -f2 -d ";"`

  for i in $(echo $seat | tr "," "\n")
  do
    valid=$(check_validity $NUM $i)
    if [ $valid -eq 1 ];
    then
      if [ -z "$validSeats" ]
      then
        validSeats="$i"
      else
        validSeats="$validSeats,$i"
      fi
      count=$((count+1))
    else
      echo -en "\e[1;31mCan't buy shit for $i!!!!\e[0m" 
    fi
  done

  movie_data $NUM $validSeats

  PERTICKETPRICE=5
  price=$((count*$PERTICKETPRICE))
  echo -e "Total ticket(s) price for \e[1;31m$count\e[0m tickets is \e[1;31m\$$price\e[0m."


  yn='xxx'
  while [ "$yn" != "Y" ] || [ "$yn" != "N" ]; do  
    echo -n "To confirm your seats Marked in Blue, press Y or N: "
    read yn
    case "$yn" in
      "Y")
        for i in $(echo $validSeats | tr "," "\n")
        do
          update_seat $NUM $i
        done
        ;;
      "N")
        echo "Please make up your mind."
        ;;
      *)
        echo "Unrecognized Input."
    esac
  done
	
	old_IFS=$IFS 
	IFS=$'\n'
	 
	echo
}


update_seat() {

  count=`sed -n $1,$1p seating.txt | cut -f1 -d ";"`
  initialcount=$count
  seats=`sed -n $1,$1p seating.txt | cut -f2 -d ";"`

  count=$((count-1))
  seats="$seats,$2"
  sed -i "$1s/.*/$count;$seats/" seating.txt

  if [ $initialcount -eq 1 ]  
  then
    sed -i "$1s/,Yes/,No/" movielist.txt
  fi
}

check_validity() {
  seats=`sed -n $1,$1p seating.txt | cut -f2 -d ";"`
  if [ `echo $seats | grep -c "$2"` -gt 0 ]
  then
    echo "0" 
  else
    echo "1"
  fi
}



# Search by screening time or movie title
search_stmt()
{
	old_IFS=$IFS
	IFS=$'\n'
	echo ""
	echo -en "Enter Screening Time or Movie Title: "
	read searchsm
	
	print_movie_header
	for LINE in `grep $searchsm movielist.txt`
	do
		print_movie $LINE
	done

 IFS=$old_IFS
	
}

# Main menu navigation
run_menu()
{
	i=-1
  while [ "$i" != "5" ]; do
		display_menu
		read i
		i=`echo $i | tr '[A-Z]' '[a-z]'`

		case "$i" in 
	  	"1")
			list_movies
			;;
		"2")
			bookmovie_fast
			;;
		"3")
			select_seat
			;;
		"4")
			search_stmt
			;;
		"5")
			echo -e "\e[1;32m GoodBye, Have A Nice Day!! \e[0m"
			echo ""		
			exit 0
			;;
		*)
			echo "Unrecognised Input."
			;;
		esac
  done
}


#----------------------------------------------#
# ---------PROGRAM SCRIPT START HERE---------- #
#----------------------------------------------#

if [ ! -f movielist.txt ]; then
  echo "Error: Movie list is not available"
fi


run_menu